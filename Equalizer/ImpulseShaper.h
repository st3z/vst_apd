//
//  ImpulseShaper.h
//  test_matrix
//
//  Created by Toshiba Laptop on 19/10/14.
//  Copyright (c) 2014 Toshiba Laptop. All rights reserved.
//

#ifndef test_matrix_ImpulseShaper_h
#define test_matrix_ImpulseShaper_h
#include "ImpulseTransformer.h"


class ImpulseShaper {
    
private:
    
    string path;
    string leftFilename;
    string rightFilename;
    ImpulseTransformer *transformer;
    
public:
    
    ImpulseShaper();
    
    Impulse* shapeImpulse(Impulse* imp, int elevation, int azimuth);
    };


#endif /* defined(__test_matrix__ImpulseShaper__) */
