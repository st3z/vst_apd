//
//  Convolver.h
//  test_matrix
//
//  Created by Toshiba Laptop on 25/10/14.
//  Copyright (c) 2014 Toshiba Laptop. All rights reserved.
//

#ifndef __test_matrix__Convolver__
#define __test_matrix__Convolver__

#include <iostream>
#include "Impulse.h"

class Convolver{
public:
    Convolver();
    void convolve(float** input, float** output, kiss_fft_cpx** impulseFfts, int32_t input_size);
    void isOverlapEmpty(bool state);
private:
    int saveCounter;
    kiss_fft_cfg cfg_test;
    kiss_fft_cfg cfg_test_inv;
    kiss_fft_cpx **timeBlock;
    kiss_fft_cpx **freqBlock;
    kiss_fft_cpx **ifftBlock;
    kiss_fft_cpx **convolutionBlock;
    float **overlap;
    bool overlapEmpty;
    int fftsize, overlapSize, convsize; // transform and convolution sizes
    void complexMultiply(kiss_fft_cpx **ffts);
    void fillAndZeroPad(float** inputsBuffer);
    void normalizeGain(kiss_fft_cpx *buffer,float max);
    void divideByFftSize(kiss_fft_cpx** fftout);
    void multiplyByFftSize(kiss_fft_cpx** fftout);
    void convolveBlock(kiss_fft_cpx** impulseFfts);
    float findMaxSampleValue(kiss_fft_cpx *buffer, int size);
    void clearOverlap();
    void saveIfftBlockTail();
};

#endif /* defined(__test_matrix__Convolver__) */
