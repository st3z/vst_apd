/* this is the base class header file */
#include "audioeffectx.h"
#include "Controller.h"
#include <cstring>


struct Program { // STRUTTURA PER MEMORIZZARE UN PRESET
	float elevation, azimuth;
   char name[32];
   Program(){ elevation = 0; azimuth =0; strcpy(name,"default");}
};


class Plugin : public AudioEffectX
{
  Program progs[8];
  float elevation,azimuth;
    Controller* controller;
    int reset;
 public:
 /* initialization and termination */
  Plugin (audioMasterCallback audioMaster);
  ~Plugin ();
  
 /* processing */
  void processReplacing (float** inputs, float** outputs, 
VstInt32 sampleFrames);
 void setParameter (VstInt32 index, float value);
float getParameter (VstInt32 index);
void  setProgram(VstInt32 program);
    
 /* FUNZIONI PER SETTARE IL LAYOUT DEI PARAMETRI */
void setProgramName (char *name);
bool getProgramNameIndexed (VstInt32 category,
                    VstInt32 index, char* text);
void getParameterLabel (VstInt32 index, char* label);
void getParameterDisplay (VstInt32 index, char* text);
void getParameterName (VstInt32 index, char* text);
    
    
bool getEffectName (char* name) {
         strcpy (name, "doublepan"); return true; }
bool getVendorString (char* text) {
         strcpy (text, "Marco Bertola"); return true; }
bool getProductString (char* text) {
         strcpy (text, "doublepan"); return true; }

    VstInt32 getVendorVersion () { return 1024; }
VstPlugCategory getPlugCategory () { return kPlugCategSpacializer; }



};
