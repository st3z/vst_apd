//
//  ImpulseCalculator.cpp
//  Equalizer
//
//  Created by Toshiba Laptop on 09/11/14.
//  Copyright (c) 2014 Marco Bertola. All rights reserved.
//

#include "ImpulseCalculator.h"

ImpulseCalculator::ImpulseCalculator(){
    
    }

void ImpulseCalculator::boundaries(float ele, float azi)
{
    if(ele<-40.f)
        ele = -40.f;
    
    if(ele>90.f)
        ele = 90.f;

    int posto = ele/10+4;
    double intpart;
    float deltaAzi1 = (360.0 / datiA[posto]);
    quad[0][0] = datiE[posto];
    quad[0][1] = (modf(azi - fmod(azi, deltaAzi1), &intpart)) < 0.5 ? intpart : intpart+1;
    
    quad[1][0] = quad[0][0];
    quad[1][1] = quad[0][1] + (modf(deltaAzi1, &intpart) < 0.5 ? (int)deltaAzi1 : (int)deltaAzi1+1);
    
    float deltaAzi2 = (360.0 / datiA[posto+1]);
    quad[2][0] = datiE[posto+1];
    quad[2][1] = (modf(azi - fmod(azi, deltaAzi2), &intpart)) < 0.5 ? intpart : intpart+1;
    
    quad[3][0] = quad[2][0];
    quad[3][1] = quad[2][1] + (modf(deltaAzi2, &intpart) < 0.5 ? (int)deltaAzi2 : (int)deltaAzi2+1);
    
    float elex, eley, elez;
    elex = 1.4 * cos(ele * M_PI/180.) * cos(azi * M_PI/180.);
    eley = 1.4 * cos(ele * M_PI/180.) * sin(azi * M_PI/180.);
    elez = 1.4 * sin(ele * M_PI/180.);
    for (int i = 0; i<4; i++) {
        quad[i][2] = 1.4 * cos(quad[i][0] * M_PI/180.) * cos(quad[i][1] * M_PI/180.);
        quad[i][3] = 1.4 * cos(quad[i][0] * M_PI/180.) * sin(quad[i][1] * M_PI/180.);
        quad[i][4] = 1.4 * sin(quad[i][0] * M_PI/180.);
    }
    
    float dist[4];
    for (int i = 0; i<4; i++) {
        dist[i] = sqrt(pow(elex-quad[i][2],2.) + pow(eley-quad[i][3],2.) + pow(elez-quad[i][4],2.));

    }
    
    int ptemp=0;
    float temp=0;
    for (int i = 0; i<4; i++) {
        for (int j=i; j<4; j++) {
            if (dist[j]>temp) {
                temp = dist[j];
                ptemp = j;
            }
        }
    }
    
    for (int j = 0; j<5; j++) {
        quad[4][j] = quad[ptemp][j];
    }
    
    elevation = quad[4][0];
    azimuth = quad[4][1];
    
}

int ImpulseCalculator::getElevation(){
    return elevation;
}
int ImpulseCalculator::getAzimuth(){
    return azimuth;
}
