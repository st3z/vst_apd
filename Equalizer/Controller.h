//
//  Controller.h
//  test_matrix
//
//  Created by Toshiba Laptop on 19/10/14.
//  Copyright (c) 2014 Toshiba Laptop. All rights reserved.
//

#ifndef test_matrix_Controller_h
#define test_matrix_Controller_h

#include <iostream>
#include "MatrixHelper.h"
#include "ImpulseShaper.h"
#include "Convolver.h"


class Controller {
    
private:
    bool impulseSet;
    MatrixHelper* matrixHelper;
    ImpulseShaper* shaper;
    Convolver* convolver;
    Impulse* currentImpulse;
    kiss_fft_cpx** impulseFfts;
public:
    Controller();
    ~Controller();
    Impulse* getImpulse(int elevation,int azimuth);
    MatrixHelper* getMatrixHelper();
    Convolver* getConvolver();
    void convolve(float** inputs,float** outputs, int elevation, int azimuth,int32_t vecframes);
};


#endif /* defined(__test_matrix__Controller__) */
