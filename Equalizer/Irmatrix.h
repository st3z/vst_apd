//
//  irmatrix.h
//  Panner
//
//  Created by Toshiba Laptop on 02/10/14.
//  Copyright (c) 2014 Stefano Artuso. All rights reserved.
//
// Impulse Response Matrix with all the impulses of dummy head database. It is implemented as a 180x360 matrix array
// 1° column identify elevation and rows start from 2° position identify the azimuth position
#ifndef test_matrix_Irmatrix_h
#define test_matrix_Irmatrix_h


#include "Impulse.h"
#include <stdio.h>



class Irmatrix {
    
private:
    Impulse matrix[ELEVATION][AZIMUTH];
    Impulse *p_matrix;
public:
    Irmatrix();
    Impulse* getMatrix();
    void setMatrix(Impulse *m);
    //   void insertImpulse(Impulse impulse);
    //   Impulse getImpulse(int row,int column);
    
};
#endif