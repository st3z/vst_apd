//
//  ImpulseShaper.cpp
//  Equalizer
//
//  Created by Toshiba Laptop on 14/10/14.
//  Copyright (c) 2014 Marco Bertola. All rights reserved.
//

#include "ImpulseShaper.h"


ImpulseShaper::ImpulseShaper(){
    
    transformer = new ImpulseTransformer();
    
}


Impulse* ImpulseShaper::shapeImpulse(Impulse* imp,int elevation, int azimuth){
    
    string hemisphere="R";
    
    if(azimuth < 180)
        hemisphere="L";
    
    imp->setAzimuth(azimuth);
    imp->setElevation(elevation);
    imp->setHemisphere(hemisphere);
    
    transformer->doImpulseFftOf(imp);
    
    return imp;
    
}

//
//void ImpulseShaper::setFileInfo(SF_INFO info_){
//    info = info_;
//}

