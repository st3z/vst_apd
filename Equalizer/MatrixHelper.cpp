//
//  MatrixHelper.cpp
//  test_matrix
//
//  Created by Toshiba Laptop on 19/10/14.
//  Copyright (c) 2014 Toshiba Laptop. All rights reserved.
//

#include "MatrixHelper.h"


MatrixHelper::MatrixHelper(){
    matrix = new Irmatrix();
    p_matrix = matrix->getMatrix();
}

void MatrixHelper::insertImpulseIntoMatrix(Impulse *imp, int el, int az){
    
    *(p_matrix+(el*AZIMUTH+az)) = *imp;
    
}

Impulse* MatrixHelper::getPmatrix(){
    return p_matrix;
}

Impulse* MatrixHelper::getImpulse(int elevation, int azimuth){
    return (p_matrix+(elevation*AZIMUTH+azimuth));
}