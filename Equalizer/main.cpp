//
//  main.cpp
//  test_matrix
//
//  Created by Toshiba Laptop on 15/10/14.
//  Copyright (c) 2014 Toshiba Laptop. All rights reserved.
//

#include <iostream>
#include "Controller.h"
#include "kiss_fft.h"
#include "_kiss_fft_guts.h"
int main(int argc, const char * argv[])
{

    // insert code here...
    
    Controller* controller = new Controller;
    Impulse* p_m = (controller->getMatrixHelper())->getPmatrix();
    Impulse i;

    //test open wav
    
    
    for(int e =0; e<ELEVATION; e++)
    {
        
        for(int a=0; a<AZIMUTH; a++)
            {
                i = *(p_m+e*AZIMUTH+a);
                
                if(i.getHasBothFiles())
                {

                    cout <<"["<< e-90 << " " << a << "]" << " : ";
                    cout<< i.getLeftFileName() << " " << i.getRightFileName()<< " ";
                    cout<<"frames L: "<<i.getLeftEarFileInfo().frames << " frames R:" << i.getRightEarFileInfo().frames<< endl;
                    
                
                }
            
            }
    }
    
//    SNDFILE* snd = i.getLeftEarFile();
    
    return 0;
}

