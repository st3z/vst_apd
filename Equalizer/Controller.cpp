//
//  Controller.cpp
//  test_matrix
//
//  Created by Toshiba Laptop on 19/10/14.
//  Copyright (c) 2014 Toshiba Laptop. All rights reserved.
//

#include "Controller.h"


Controller::Controller() {
    impulseSet=false;
    matrixHelper = new MatrixHelper;       //E' come scrivere p_matrix=*(matrix) oppure *(matrix+0) oppure *matrix
    shaper = new ImpulseShaper;
    convolver = new Convolver;
    currentImpulse = nullptr;
    impulseFfts = new kiss_fft_cpx*[2];
    int e,a;
    for(e=0; e<ELEVATION; e++)
        for(a=0; a<AZIMUTH; a++)
        {
            Impulse newImp;
            shaper->shapeImpulse(&newImp, e, a);
            matrixHelper -> insertImpulseIntoMatrix(&newImp, e, a);
            //cout<<"e:"<<e<<" a:"<<a<<endl;
            //cout << ((matrixHelper->getPmatrix())+(e*AZIMUTH+a))->getLeftFileName() << endl;
        }
}

Controller::~Controller(){
    /*delete matrixHelper;
    delete shaper;
    delete convolver;*/
}
Impulse* Controller::getImpulse(int elevation, int azimuth){
    if(azimuth<=0)
        return matrixHelper->getImpulse(elevation+90, abs(azimuth));
    return matrixHelper->getImpulse(elevation+90,360-azimuth );
    
}

MatrixHelper* Controller::getMatrixHelper(){
    return matrixHelper;
}

void Controller::convolve(float** inputs,float** outputs, int elevation, int azimuth,int32_t vecframes){
        
    if(getImpulse(elevation, azimuth)->hasBothFft())
    {
        currentImpulse = getImpulse(elevation, azimuth);
    }

    if(currentImpulse!=nullptr)
    {
            impulseFfts[0] = currentImpulse->getLeftEarFftBuffer();
            impulseFfts[1] = currentImpulse->getRightEarFftBuffer();
            convolver->convolve(inputs, outputs,
                                impulseFfts,
                                vecframes);
    }
}

Convolver* Controller::getConvolver(){
    return convolver;
}