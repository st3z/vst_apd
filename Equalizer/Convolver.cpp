//
//  Convolver.cpp
//  test_matrix
//
//  Created by Toshiba Laptop on 25/10/14.
//  Copyright (c) 2014 Toshiba Laptop. All rights reserved.
//

//#include <math.h>
#include "Convolver.h"


//TODO: globalizzare le variabili come tutti i buffer

Convolver::Convolver(){
    overlapEmpty = true;
    overlapSize = IMPULSE_SIZE-1;
    
    overlap = new float*[2];
    timeBlock = new kiss_fft_cpx*[2];
    freqBlock = new kiss_fft_cpx*[2];
    convolutionBlock = new kiss_fft_cpx*[2];
    ifftBlock = new kiss_fft_cpx*[2];
    
    for(int i=0; i<2; i++)
    {
        overlap[i] = new float[overlapSize];
        timeBlock[i] = new kiss_fft_cpx[FFTSIZE];
        freqBlock[i] = new kiss_fft_cpx[FFTSIZE];
        ifftBlock[i] = new kiss_fft_cpx[FFTSIZE];
        convolutionBlock[i] = new kiss_fft_cpx[FFTSIZE];
    }
    cfg_test = kiss_fft_alloc(FFTSIZE, 0, NULL, NULL);
    cfg_test_inv = kiss_fft_alloc(FFTSIZE, 1, NULL, NULL);
    
}
float Convolver::findMaxSampleValue(kiss_fft_cpx *buffer, int size){
    float max=0;
    for (int i = 0; i < size; i++)
    {
        if(fabs(buffer[i].r)>=max)
                max = buffer[i].r;
            
	}
    
    return max;
	
}

void Convolver::fillAndZeroPad(float** buffer){
    for (int i = 0; i < FFTSIZE; i++)
    {
		if (i < IMPULSE_SIZE)
        {
            timeBlock[0][i].r = buffer[0][i];
            timeBlock[0][i].i = 0.f;
            
            timeBlock[1][i].r = buffer[0][i];
            timeBlock[1][i].i = 0.f;
            
            
        }
		else
        {
            timeBlock[0][i].r = 0.f;
            timeBlock[0][i].i = 0.f;
            
            timeBlock[1][i].r = 0.f;
            timeBlock[1][i].i = 0.f;
            
        }
	}
//    for(int i=0;i<IMPULSE_SIZE;i++)
//    {
//        cout.precision(20);
//        cout<<i<<": 0: "<<buffer[0][i];
//        
//        cout.precision(20);
//        cout<<i<<":   1: "<<buffer[1][i]<<endl;
//    }
}

void Convolver::normalizeGain(kiss_fft_cpx *buffer,float max){
    
    for(int i=0; i<FFTSIZE; i++)
    {
        
        if(buffer[i].r >= max)
            buffer[i].r = buffer[i].r/((float)(max+0.0001));
    }
    
}

void Convolver::divideByFftSize(kiss_fft_cpx** ffts){
    
    for(int i=0; i<FFTSIZE; i++)
    {
        
        ffts[0][i].r = ffts[0][i].r/float(FFTSIZE);
        ffts[0][i].i = ffts[0][i].i/float(FFTSIZE);
        
        ffts[1][i].r = ffts[1][i].r/float(FFTSIZE);
        ffts[1][i].i = ffts[1][i].i/float(FFTSIZE);
    }
    
    
}
void Convolver::multiplyByFftSize(kiss_fft_cpx** iffts){
    
    for(int i=0; i<FFTSIZE; i++)
    {
        
        iffts[0][i].r = iffts[0][i].r*float(FFTSIZE);
        iffts[0][i].i = iffts[0][i].i*float(FFTSIZE);
        
        iffts[1][i].r = iffts[1][i].r*float(FFTSIZE);
        iffts[1][i].i = iffts[1][i].i*float(FFTSIZE);
        
    }
    
    
}

void Convolver::complexMultiply(kiss_fft_cpx **ffts){
    //(a+ib)*(c+id) = (ac - bd) + (ad + bc)i
    
    for (int j = 0; j < FFTSIZE; j ++)
    {
        convolutionBlock[0][j].r = (freqBlock[0][j].r*ffts[0][j].r - freqBlock[0][j].i*ffts[0][j].i);
        convolutionBlock[0][j].i = (freqBlock[0][j].r*ffts[0][j].i + freqBlock[0][j].i*ffts[0][j].r);
        
        convolutionBlock[1][j].r = (freqBlock[1][j].r*ffts[1][j].r - freqBlock[1][j].i*ffts[1][j].i);
        convolutionBlock[1][j].i = (freqBlock[1][j].r*ffts[1][j].i + freqBlock[1][j].i*ffts[1][j].r);
    }
    /*result->r = ((a->r)/8)*((b->r)/8) - ((a->i)/8)*((b->i)/8);
     result->i = ((a->r)/8)*((b->i)/8) + ((a->i)/8)*((b->r)/8);
     */
    
}

void Convolver::clearOverlap(){
    for(int i=0;i<0;i++)
        overlap[0][0]=overlap[1][1]=0.f;
}

void Convolver::isOverlapEmpty(bool state){
    overlapEmpty = state;
}

void Convolver::convolveBlock(kiss_fft_cpx** impulseFft){

    
    // Take the DFT of input signal block
    kiss_fft(cfg_test, timeBlock[0], freqBlock[0]);
    kiss_fft(cfg_test, timeBlock[1], freqBlock[1]);

    //normalize the fft of freqBlock
    //OBBLIGATORIO
    divideByFftSize(freqBlock);
    
    
    // complex multiplication
    // first pair is re[0Hz] and re[Nyquist]
    //	outspec[0] = inspec[0] * impspec[0];
    //outspec[1] = inspec[1] * impspec[1];
    // (a+ib)*(c+id) = (ac - bd) + (ad + bc)i
    //			
    complexMultiply(impulseFft);
    
    
    // IDFT of the spectral product
    kiss_fft(cfg_test_inv, convolutionBlock[0], ifftBlock[0]);
    kiss_fft(cfg_test_inv, convolutionBlock[1], ifftBlock[1]);

    multiplyByFftSize(ifftBlock);
    
    //normalizeGain(ifftBlock, findMaxSampleValue(ifftBlock, FFTSIZE));
    // zero the sample counter
    
}

void Convolver::saveIfftBlockTail(){
    int j=0;
    for(int i=IMPULSE_SIZE;i<=CONVSIZE;i++,j++)
    {
        overlap[0][j] = ifftBlock[0][i].r;
        overlap[1][j] = ifftBlock[1][i].r;
    }
    overlapEmpty=false;
}

void Convolver::convolve(float** input, float** output, kiss_fft_cpx** impulseFfts, int32_t input_size) {
    
    
    fillAndZeroPad(input);
    /*
    cout<<"convolver"<<endl;
    for(int k =0; k<FFTSIZE; k++)
    {
        cout<<"Conv[R]: "<<k<<" "<<impulseFft[k].r<<"   ";
        cout<<"Conv[I]: "<<k<<" "<<impulseFft[k].i<<endl;
    }
    
    cout<<"/convolver"<<endl;
    */
    convolveBlock(impulseFfts);
    
    if(overlapEmpty)
    {
        saveIfftBlockTail();
        for(int i=0;i<IMPULSE_SIZE;i++)
        {
            output[0][i]=ifftBlock[0][i].r;
            output[1][i]=ifftBlock[1][i].r;
        }
    }
    
    else
    {
        
        for(int i=0;i<IMPULSE_SIZE;i++)
        {
            output[0][i]=overlap[0][i]+ifftBlock[0][i].r;
            output[1][i]=overlap[1][i]+ifftBlock[1][i].r;
            output[0][i]=output[0][i]/(fabsf(output[0][i])+1);
            output[1][i]=output[1][i]/(fabsf(output[1][i])+1);
            
        }
        //clearOverlap();
        saveIfftBlockTail();
    }
    
	// de-allocate memory
	/*delete[] overlap;
	delete[] ifftBlock;
	delete[] timeBlock;*/
    //TODO: finire i DELETE (se mai servissero)

}
