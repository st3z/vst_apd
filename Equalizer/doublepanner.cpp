/* this is our plugin class header file */
#include "doublepanner.h"
#include <cstdio>

AudioEffect* createEffectInstance(audioMasterCallback audioMaster)
{
    return new Plugin(audioMaster);
}


Plugin::Plugin (audioMasterCallback audioMaster)
  : AudioEffectX (audioMaster, 8, 3){
      controller = new Controller();
      elevation = 0.f;
      azimuth = 0.f;
      reset= 0;
      setNumInputs(1);
      setNumOutputs(2);
      canProcessReplacing();
}

Plugin::~Plugin() { }

void Plugin::processReplacing(float** inputs,
              float** outputs, VstInt32 vecframes)
{
    //float* in = inputs[0];
    controller->convolve(inputs,outputs,elevation,azimuth,vecframes);
} 

void Plugin::setParameter (VstInt32 index, float value){

    switch(index)
    { // ATTENZIONE A NON SCAMBIARE GLI INDICI!!!!
      case 0:
      progs[curProgram].elevation = elevation = (int)((value*180)-90);
            return;
        case 1:
            progs[curProgram].azimuth = azimuth = (int)((value*360)-180);
            return;
        case 2:
            reset = (int)(value);
            return;
	default:
	return;
    }
}


float Plugin::getParameter (VstInt32 index) { 

switch(index){  // ATTENZIONE A NON SCAMBIARE GLI INDICI!!!!
        case 0:
        return elevation;
    case 1:
        return azimuth;
    case 2:
        return reset;
	  default:
	  return 0;
      }
}

void Plugin::setProgramName (char *name)
{
  strcpy(progs[curProgram].name, name);
}


bool Plugin::getProgramNameIndexed(VstInt32 category, 
             VstInt32 index, char* text)
{
    switch (index) {
        case 0:
            strcpy(text, "first program");
            return true;
        case 1:
            strcpy(text, "second program");
            return true;
        case 2:
            strcpy(text, "third program");
            return true;
        case 3:
            strcpy(text, "fourth program");
            return true;
            
        default:
            break;
    }
  return false;
}

void Plugin::getParameterName(VstInt32 index, char *label)
{
  switch (index)
    {
    case 0: 
      strcpy (label, "elevation");
      break;
    case 1: 
      strcpy (label, "azimuth");
      break;
 
    case 2:
    strcpy (label, "RESET");
    break;
    }
}

void Plugin::getParameterLabel (VstInt32 index, char *label)
{
      strcpy (label, "position");  
     
}

void Plugin::getParameterDisplay (VstInt32 index, char *text) // FUNZIONE CHIAMATA DALL'HOST PER VISUALIZZARE IL VALORE DEL PARAMETRO
{
    switch (index)
    {
    case 0: 
      int2string (elevation, text, kVstMaxParamStrLen);
      break;
    case 1: 
      int2string (azimuth, text, kVstMaxParamStrLen);
      break;
    case 2:
            int2string(reset, text, kVstMaxParamStrLen);
            break;
    }
}

void Plugin::setProgram(VstInt32 program){ // FUNZIONE CHE CHIAMA L'HOST PER SETTARE UN PRESET
    
    switch (program) {
        case 0:
            setParameter(0,1.0);
            setParameter(1,1.0);
            break;
        case 1:
            setParameter(0,0.5);
            setParameter(1,0.5);
            break;
        case 2:
            setParameter(0,0.0);
            setParameter(1,0.0);
            break;
        case 3:
            setParameter(0,0.0);
            setParameter(1,1.0);
            break;
            
        default:
            setParameter(0,0.5);
            setParameter(1,0.5);
            break;
    }
    
    curProgram = program;

}
