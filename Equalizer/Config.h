//
//  Config.h
//  test_matrix
//
//  Created by Toshiba Laptop on 02/11/14.
//  Copyright (c) 2014 Toshiba Laptop. All rights reserved.
//

#ifndef test_matrix_Config_h
#define test_matrix_Config_h
#define IMPULSE_SIZE 512
#define AZIMUTH 361
#define ELEVATION 181
#define ROOTPATH "/Users/Toshiba/Documents/irs/"
#define FFTSIZE 1024
#define CONVSIZE 1023


#endif
