//
//  impulse.h
//  Panner
//
//  Created by Toshiba Laptop on 02/10/14.
//  Copyright (c) 2014 Marco Bertola. All rights reserved.
//

#ifndef test_matrix_Impulse_h
#define test_matrix_Impulse_h


#include "kiss_fft.h"
#include "Config.h"
#include <iostream>
using namespace std;

class Impulse {
    
private:
    int azimuth;
    int elevation;
    bool bothFft = false;
    string hemisphere;
    kiss_fft_cpx* leftEarFftBuffer;
    kiss_fft_cpx* rightEarFftBuffer;
    string leftFileName;
    string rightFileName;

public:
    Impulse();
    Impulse(int elevation_,int azimuth_, char* hemisphere_, kiss_fft_cpx* leftEarFftBuffer_, kiss_fft_cpx* rightEarFftBuffer_);
    string getHemisphere();
    int getAzimuth();
    int getElevation();
    string getLeftFileName();
    string getRightFileName();
    kiss_fft_cpx* getLeftEarFftBuffer();
    kiss_fft_cpx* getRightEarFftBuffer();
    bool hasBothFft();
    void setAzimuth(int new_azimuth);
    void setElevation(int new_elevation);
    void setHemisphere(string new_hemisphere);
    void setHasBothFft(bool state);
    void setLeftFileName(string leftName);
    void setRightFileName(string rightName);
    void setLeftEarFftBuffer(kiss_fft_cpx* leftEarFftBuffer);
    void setRightEarFftBuffer(kiss_fft_cpx* rightEarFftBuffer);
    bool isEmpty();

};
#endif