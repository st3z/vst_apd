//
//  MatrixHelpr.h
//  test_matrix
//
//  Created by Toshiba Laptop on 21/10/14.
//  Copyright (c) 2014 Toshiba Laptop. All rights reserved.
//

#ifndef test_matrix_MatrixHelper_h
#define test_matrix_MatrixHelper_h

#include <iostream>
#include "Irmatrix.h"


class MatrixHelper{
    
private:
    Irmatrix* matrix;
    Impulse* p_matrix;
public:
    MatrixHelper();
    void insertImpulseIntoMatrix(Impulse *imp, int el, int az);
    Impulse* getPmatrix();
    Impulse* getImpulse(int elevation, int azimuth);
    
};
#endif