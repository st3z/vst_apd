//
//  impulse.cpp
//  Panner
//
//  Created by Toshiba Laptop on 02/10/14.
//  Copyright (c) 2014 Stefano Artuso. All rights reserved.
//  This class implements an Impulse which consists of 2 file for left and rigth channel. It's just a container, for all infos about where to place the sound. It contains: azimuth, elevation and hemisphere. The first two values are rapresented in degrees, the first of them identify the horizontal plan, the latter the vertical one. The hemisphere identify where the sound will be listen and it could be left (L) or right (R).

//  Is assumed that 0° degree in horizontal plan is in front of the head and 180° behind. So, degrees from 0° to 179° are on the left hemisphere (from the front to the back) and from 180 to 360° are on the right one (from the back to the front) 

#include "Impulse.h"


Impulse::Impulse(){
    
    bothFft = false;
}

Impulse::Impulse(int elevation_,int azimuth_, char* hemisphere_, kiss_fft_cpx* leftEarBuffer_, kiss_fft_cpx* rightEarBuffer_){
    
    elevation = elevation_;
    azimuth = azimuth_;
    hemisphere = hemisphere_;
    
    leftEarFftBuffer = leftEarBuffer_;
    rightEarFftBuffer = rightEarBuffer_;

    bothFft = false;

}


int Impulse::getAzimuth(){
    return azimuth;
}

int Impulse::getElevation(){
    return elevation;
}

string Impulse::getHemisphere(){
    return hemisphere;
}

bool Impulse::hasBothFft(){
    return bothFft;
}

string Impulse::getLeftFileName(){
    return leftFileName;
}

string Impulse::getRightFileName(){
    return rightFileName;
}

kiss_fft_cpx* Impulse::getLeftEarFftBuffer(){
    return leftEarFftBuffer;
}

kiss_fft_cpx* Impulse::getRightEarFftBuffer(){
    return rightEarFftBuffer;
}


void Impulse::setAzimuth(int new_azimuth){
    azimuth = new_azimuth;
}

void Impulse::setElevation(int new_elevation){
    elevation = new_elevation;
}

void Impulse::setHemisphere(string new_hemisphere){
    hemisphere = new_hemisphere;
}


void Impulse::setHasBothFft(bool state){
    bothFft = state;
}

void Impulse::setLeftFileName(string leftName){
    leftFileName = leftName;
}

void Impulse::setRightFileName(string rightName){
    rightFileName = rightName;
}


void Impulse::setLeftEarFftBuffer(kiss_fft_cpx* leftEarFftBuffer_){
    leftEarFftBuffer = leftEarFftBuffer_;
}
void Impulse::setRightEarFftBuffer(kiss_fft_cpx* rightEarFftBuffer_){
    rightEarFftBuffer = rightEarFftBuffer_;
}

bool Impulse::isEmpty(){
    return leftEarFftBuffer ==NULL && rightEarFftBuffer == NULL;
}