//
//  Transformer.cpp
//  test_matrix
//
//  Created by Toshiba Laptop on 31/10/14.
//  Copyright (c) 2014 Toshiba Laptop. All rights reserved.
//

#include "ImpulseTransformer.h"

ImpulseTransformer::ImpulseTransformer(){
    int convsize = (IMPULSE_SIZE*2) -1;
    fftSize =1;
    while(fftSize< convsize)
        fftSize *= 2;
    
    leftImpulseSamples = new float[IMPULSE_SIZE];
    rightImpulseSamples = new float[IMPULSE_SIZE];
    leftImpulseTimeComplex = new kiss_fft_cpx[FFTSIZE];
    rightImpulseTimeComplex = new kiss_fft_cpx[FFTSIZE];
    
    cfg = kiss_fft_alloc(FFTSIZE, 0, NULL, NULL);
    

}

float ImpulseTransformer::getMaxSample(float* impulseSamples){
    float max= 0.f;
    for(int i=0; i<IMPULSE_SIZE; i++)
    {
        if(fabs(impulseSamples[i])>= max)
            max = impulseSamples[i];
    }
    
    return max;

}

void ImpulseTransformer::moveNormalizedSamplesTo(kiss_fft_cpx *impulseTimeComplex,float* impulseSamples, float maxValue){
    for (int i = 0; i < FFTSIZE; i++)
    {
		if (i < IMPULSE_SIZE)
        {
            impulseTimeComplex[i].r = impulseSamples[i];
            impulseTimeComplex[i].i = 0.f;
        }
		else
        {
            impulseTimeComplex[i].r = 0.f;
            impulseTimeComplex[i].i = 0.f;
        }
	}
}

void ImpulseTransformer::divideByFftSizeBoth(kiss_fft_cpx* leftImpulseFreq,kiss_fft_cpx* rightImpulseFreq){
    
    
    for(int i=0; i<FFTSIZE; i++)
    {
        
        leftImpulseFreq[i].r = leftImpulseFreq[i].r/(float)FFTSIZE;
        leftImpulseFreq[i].i = leftImpulseFreq[i].i/(float)FFTSIZE;
        
        rightImpulseFreq[i].r = rightImpulseFreq[i].r/(float)FFTSIZE;
        rightImpulseFreq[i].i = rightImpulseFreq[i].i/(float)FFTSIZE;
    }
    

    
}



void ImpulseTransformer::doImpulseFftOf(Impulse* imp){
    int azimuth = imp->getAzimuth();
    int elevation = imp->getElevation()-90;
    string hemisphere = imp->getHemisphere();
    
    
    string zeros="";
    string zerosForMirroredAngle="";
    if(azimuth>=0 && azimuth<10)
        zeros="00";
    else if(azimuth>=10&&azimuth<100)
        zeros ="0";
    else if (azimuth>350)
        zerosForMirroredAngle="00";
    else if (azimuth>260)
        zerosForMirroredAngle="0";
    
    
    SF_INFO *leftImpulseInfo = new SF_INFO;
    SF_INFO *rightImpulseInfo = new SF_INFO;
    string path;
    if(hemisphere == "R")
    {
        imp->setLeftFileName(hemisphere+to_string(elevation)+"e"+zeros+to_string(azimuth)+"a.wav");
        path =(ROOTPATH+(imp)->getLeftFileName());
        
        leftEarImpulseFile = sf_open( (ROOTPATH+(imp)->getLeftFileName()).c_str(), SFM_READ,leftImpulseInfo) ;
        
        imp->setRightFileName(hemisphere+to_string(elevation)+"e"+zerosForMirroredAngle+to_string(360-azimuth)+"a.wav");
        rightEarImpulseFile = sf_open( (ROOTPATH+(imp)->getRightFileName()).c_str(), SFM_READ, rightImpulseInfo) ;
        
    }
    else
    {
        
        imp->setLeftFileName(hemisphere+to_string(elevation)+"e"+zerosForMirroredAngle+to_string(360-azimuth)+"a.wav");
        path =(ROOTPATH+(imp)->getLeftFileName());
        
        leftEarImpulseFile = sf_open( (ROOTPATH+(imp)->getLeftFileName()).c_str(), SFM_READ,leftImpulseInfo) ;
        
        imp->setRightFileName(hemisphere+to_string(elevation)+"e"+zeros+to_string(azimuth)+"a.wav");
        rightEarImpulseFile = sf_open( (ROOTPATH+(imp)->getRightFileName()).c_str(), SFM_READ, rightImpulseInfo) ;
        
    }
    
    if(rightEarImpulseFile!=NULL&&leftEarImpulseFile!=NULL)
    {
        leftImpulseFreqComplex = new kiss_fft_cpx[FFTSIZE];
        rightImpulseFreqComplex = new kiss_fft_cpx[FFTSIZE];
        sf_read_float(leftEarImpulseFile, leftImpulseSamples, IMPULSE_SIZE);
        sf_read_float(rightEarImpulseFile, rightImpulseSamples, IMPULSE_SIZE);
        
        sf_close(leftEarImpulseFile);
        sf_close(rightEarImpulseFile);
        
        moveNormalizedSamplesTo(leftImpulseTimeComplex, leftImpulseSamples, getMaxSample(leftImpulseSamples));
        moveNormalizedSamplesTo(rightImpulseTimeComplex, rightImpulseSamples, getMaxSample(rightImpulseSamples));
        
        
        kiss_fft(cfg, leftImpulseTimeComplex, leftImpulseFreqComplex); //CFG - IN - OUT
        kiss_fft(cfg, rightImpulseTimeComplex, rightImpulseFreqComplex);
        
        divideByFftSizeBoth(leftImpulseFreqComplex,rightImpulseFreqComplex);
        
        imp->setLeftEarFftBuffer(leftImpulseFreqComplex);
        imp->setRightEarFftBuffer(rightImpulseFreqComplex);
      /*  cout<<"ImpulseTransformer"<<endl;
        for(int k =0; k<FFTSIZE; k++)
        {
            cout<<"R[R]: "<<k<<" "<<(imp->getRightEarFftBuffer())[k].r<<"   ";
            cout<<"R[I]: "<<(imp->getRightEarFftBuffer())[k].i<<endl;
        }
        
        cout<<"/ImpulseTransformer"<<endl;*/
        if(imp->getLeftEarFftBuffer()!=NULL&&imp->getRightEarFftBuffer()!=NULL)
            imp->setHasBothFft(true);
           
    }
    //TODO: da rivedere
   // delete leftImpulseInfo;
  //  delete rightImpulseInfo;
}

int ImpulseTransformer::getFftSize(){
    return fftSize;
}
