//
//  ImpulseCalculator.h
//  Equalizer
//
//  Created by Toshiba Laptop on 09/11/14.
//  Copyright (c) 2014 Marco Bertola. All rights reserved.
//

#ifndef __Equalizer__ImpulseCalculator__
#define __Equalizer__ImpulseCalculator__

#include <iostream>
#include <math.h>
#include <iomanip>
using namespace std;


class ImpulseCalculator{
public:
    ImpulseCalculator();
    
    
    void boundaries(float ele, float azi);
    int getElevation();
    int getAzimuth();
private:
    const int datiE[14]= {-40, -30, -20, -10,  0, 10, 20, 30, 40, 50, 60, 70, 80, 90};
    const int datiA[14] = { 56,  60,  72,  72, 72, 72, 72, 60, 56, 45, 36, 24, 12,  1};
    float quad[5][5];
    int elevation,azimuth;
    
};


#endif /* defined(__Equalizer__ImpulseCalculator__) */
