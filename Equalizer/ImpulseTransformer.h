//
//  Transformer.h
//  test_matrix
//
//  Created by Toshiba Laptop on 31/10/14.
//  Copyright (c) 2014 Toshiba Laptop. All rights reserved.
//

#ifndef __test_matrix__Transformer__
#define __test_matrix__Transformer__


#include "sndfile.h"
#include "Impulse.h"


class ImpulseTransformer{
    
private:
    int fftSize;
    SNDFILE* wavfile;
    SF_INFO* info;
    float* leftImpulseSamples;
    float* rightImpulseSamples;
    kiss_fft_cpx* leftImpulseTimeComplex;
    kiss_fft_cpx* rightImpulseTimeComplex;
    kiss_fft_cpx* leftImpulseFreqComplex;
    kiss_fft_cpx* rightImpulseFreqComplex;
    kiss_fft_cfg cfg;
    
    SNDFILE *leftEarImpulseFile;
    SNDFILE *rightEarImpulseFile;
    
    SF_INFO *impulseFileInfo;

    
    float getMaxSample(float* impulseSamples);
    void divideByFftSizeBoth(kiss_fft_cpx* leftImpulseFreq,kiss_fft_cpx* rightImpulseFreq);
    void moveNormalizedSamplesTo(kiss_fft_cpx *impulse,float* buffer,float maxValue);
    
public:
    ImpulseTransformer();
    void doImpulseFftOf(Impulse* imp);
    int getFftSize();
    
    
};

#endif /* defined(__test_matrix__Transformer__) */
